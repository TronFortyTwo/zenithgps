import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import Indicator 1.0

Page {
	id: settingsPage

	property bool installed: Indicator.checkInstalled()

	header: PageHeader {
		id: header
		title: i18n.tr("ZenithGPS")

		trailingActionBar.actions: [
			Action {
				iconName: 'info'
				text: i18n.tr('About')
				onTriggered: pageStack.push(Qt.resolvedUrl('AboutPage.qml'))
			}
		]
	}

	LocationPicker {
		visible: false
		id: locationPicker
	}

	Settings {
		id: settings

		onSaved: {
			if (!success) {
				message.text = i18n.tr("Failed to save the settings");
				message.color = UbuntuColors.red;
			}
		}
	}

    PasswordPopup {
		id: passwordPopup

		onAborted: {
			loading.running = false;
		}

		onWrongPassword: {
			loading.running = false;
			message.text = i18n.tr("Wrong password! Try again");
			message.color = UbuntuColors.red;
			message.visible = true;
		}
	}

    Flickable {
        anchors {
            top: header.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        clip: true
        contentHeight: contentColumn.height + units.gu(4)

        Column {
            id: contentColumn
            anchors {
                left: parent.left
                top: parent.top
                right: parent.right
                margins: units.gu(2)
            }
            spacing: units.gu(3)

			Label {
				text: i18n.tr("ZenithGPS is currently") + " " +
					(
						loading.running ?
							"<font color='orange'>" + i18n.tr("LOADING") + "</font>" :
							(
								settingsPage.installed ?
									"<font color='green'>" + i18n.tr("ACTIVE") + "</font>":
									"<font color='red'>" + i18n.tr("INACTIVE") + "</font>"
							)
					)

				anchors.horizontalCenter: parent.horizontalCenter
				font.bold: true
			}

			Label {
				text: i18n.tr("Will stay active until next reboot")

				anchors.horizontalCenter: parent.horizontalCenter
				visible: settingsPage.installed
			}

			Rectangle {
				height: 1
				width: parent.width
				color: UbuntuColors.ash
			}

			Label {
				text: loading.running ?
					i18n.tr("Position") :
					settingsPage.installed ?
						i18n.tr("Update position") :
						i18n.tr("Set position")
				anchors.horizontalCenter: parent.horizontalCenter
				font.bold: true
			}

			Button {
				text: i18n.tr("Pick from map")
				width: parent.width * 0.5
				visible: !loading.running
				anchors.horizontalCenter: parent.horizontalCenter

				onClicked: {
					pageStack.push(locationPicker)
				}
			}

            RowLayout {
				anchors.horizontalCenter: parent.horizontalCenter
                TextField {
                    id: lat
                    readOnly: loading.running
                    validator: DoubleValidator {
                        bottom: -90
                        top: 90
                        //decimals: 8
                        locale: 'en_US'  // Force using decimals
                        notation: DoubleValidator.StandardNotation
                    }
                    inputMethodHints: Qt.ImhDigitsOnly

                    onTextChanged: {
                        settings.lat = text;
                    }

					Component.onCompleted: text = settings.lat
                }
                Label {
                    text: i18n.tr("Latitude")
                    Layout.fillWidth: true
                }
            }

            RowLayout {
				anchors.horizontalCenter: parent.horizontalCenter
                TextField {
                    id: lng
                    readOnly: loading.running
                    validator: DoubleValidator {
                        bottom: -180
                        top: 180
                        //decimals: 8
                        locale: 'en_US'  // Force using decimals
                        notation: DoubleValidator.StandardNotation
                    }
                    inputMethodHints: Qt.ImhDigitsOnly

                    onTextChanged: {
                        settings.lng = text;
                    }

					Component.onCompleted: text = settings.lng;
                }
                Label {
                    text: i18n.tr("Longitude")
                    Layout.fillWidth: true
                }
            }

            Connections {
				target: locationPicker

				onSetCoord: {
					lat.text = newlat;
					lng.text = newlng;

					message.visible = true;
					message.color = UbuntuColors.green
					message.text = "Update location to apply"
				}
			}


			Rectangle {
				height: 1
				width: parent.width
				color: UbuntuColors.ash
			}

            Label {
				anchors.horizontalCenter: parent.horizontalCenter
                id: message
                visible: false
            }

            Button {
				id: positive_button
                text: settingsPage.installed ? i18n.tr("Update location") : i18n.tr("Activate")
                width: parent.width * 3/4
                visible: !loading.running
				anchors.horizontalCenter: parent.horizontalCenter
				onClicked: {
					message.visible = true;

                    settings.lat = settings.lat.replace(',', '.')
                    lat.text = lat.text.replace(',', '.')
                    settings.lng = settings.lng.replace(',', '.')
                    lng.text = lng.text.replace(',', '.')

                    var valid = true;
                    if (!lat.acceptableInput) {
						valid = false;
                        message.text = i18n.tr("Please specify the latitude") + "<br>";
                        // TRANSLATORS: %1 is representing the min/max latitude (e.g. -90 to 90)
                        message.text += i18n.tr("within the appropriate range (-%1 to %1)").arg(lat.validator.top);
                        message.color = UbuntuColors.orange;
                    }
                    else if (!lng.acceptableInput) {
						valid = false;
                        message.text = i18n.tr("Please specify the longitude") + "<br>";
                        // TRANSLATORS: %1 is representing the min/max longitude (e.g. -180 to 180)
                        message.text += i18n.tr("within the appropriate range (-%1 to %1)").arg(lng.validator.top);
                        message.color = UbuntuColors.orange;
                    }
                    if (valid) {
						settings.save();
						passwordPopup.lat = lat.text
						passwordPopup.lon = lng.text

						loading.running = true;
						message.visible = false;
						passwordPopup.callback = "activate"
						passwordPopup.open();
					}
                }
                color: UbuntuColors.green
            }

            Button {
				id: negative_button
                text: i18n.tr("Deactivate")
                width: parent.width * 3/4
				anchors.horizontalCenter: parent.horizontalCenter
				color: UbuntuColors.orange
				visible: settingsPage.installed && !loading.running
                onClicked: {
					loading.running = true;
                    message.visible = false;
					passwordPopup.callback = "deactivate"
					passwordPopup.open();
                }
            }

			ActivityIndicator {
				width: units.gu(7)
				height: units.gu(7)
				id: loading
				anchors.horizontalCenter: parent.horizontalCenter
			}
        }
    }

    Connections {
        target: Indicator

		onInstalled: {
			message.visible = true;
			loading.running = false;
			if (success) {
				message.text = i18n.tr("Successfully activated");
				message.color = UbuntuColors.green;
			}
			else {
				message.text = i18n.tr("Failed to activate");
				message.color = UbuntuColors.red;
			}
		}

		onUninstalled: {
			message.visible = true;
			loading.running = false;
            if (success) {
                message.text = i18n.tr("Successfully deactivated");
                message.color = UbuntuColors.green;
            }
            else {
                message.text = i18n.tr("Failed to deactivate");
                message.color = UbuntuColors.red;
            }
        }

		onIsInstalledChanged: {
			settingsPage.installed = isInstalled;
		}
    }
}
