import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3

import QtLocation 5.12
import QtPositioning 5.12


Page {
	id: locationPicker
	
	property string maplng: settings.lng
	property string maplat: settings.lat
	
	signal setCoord (string newlat, string newlng)
	
	header: PageHeader {
		id: header
		title: i18n.tr("Pick location")
		
		trailingActionBar.actions: [
			Action {
				iconName: 'ok'
				text: i18n.tr('Apply')
				onTriggered: {
					setCoord(maplat, maplng);
					pageStack.pop();
				}
			}
		]
	}
	
	Plugin {
		id: mapPlugin
		name: "osm"
	}
	
	Map {
		id: map
		anchors.fill: parent
		plugin: mapPlugin
		
		gesture.acceptedGestures: MapGestureArea.PinchGesture | MapGestureArea.PanGesture | MapGestureArea.FlickGesture
		
		center: QtPositioning.coordinate(settings.lat, settings.lng)
		
		onCenterChanged: {
			maplng = map.center.longitude
			maplat = map.center.latitude
		}
		
		zoomLevel: 14
		
		Rectangle {
			anchors.horizontalCenter: parent.horizontalCenter
			height: parent.height
			width: Math.max(units.dp(1), 2)
			color: "black"
		}
		Rectangle {
			anchors.verticalCenter: parent.verticalCenter
			width: parent.width
			height: Math.max(units.dp(1), 2)
			color: "black"
		}
		Rectangle {
			anchors.centerIn: parent
			width: units.gu(4)
			height: units.gu(4)
			radius: units.gu(2)
			color: "transparent"
			
			border.width: Math.max(units.dp(1), 2)
			border.color: "black"
		}
	}
	
	onVisibleChanged: {
		map.center = QtPositioning.coordinate(settings.lat, settings.lng)
	}
}
