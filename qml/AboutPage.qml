/* Copyright 2020 Emanuele Sorce
 *
 * This file is part of Sturm Reader and is distributed under the terms of
 * the GPL. See the file COPYING for full details.
 */

import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3

Page {
	header: PageHeader {
		id: header
		title: i18n.tr("ZenithGPS")
	}

	Flickable {
		id: flickable
		anchors.fill: parent
		contentHeight:  layout.height + units.dp(80)
		contentWidth: parent.width

		Column {
			id: layout

			spacing: units.dp(35)
			anchors.top: parent.top
			anchors.left: parent.left
			anchors.right: parent.right
			width: parent.width

			Item {
				height: units.gu(10)
				width: 1
			}

			Image {
				anchors.horizontalCenter: parent.horizontalCenter
				height: width
				width: Math.min(parent.width/2, parent.height/3)
				source: Qt.resolvedUrl("../assets/logo.svg")
			}

			Label {
				anchors.horizontalCenter: parent.horizontalCenter
				width: parent.width * 4 / 5
				font.bold: true
				horizontalAlignment: Text.AlignHCenter
				wrapMode: Text.WordWrap
				text: i18n.tr("ZenithGPS")
			}

			Label {
				anchors.horizontalCenter: parent.horizontalCenter
				width: parent.width * 4 / 5
				wrapMode: Text.WordWrap
				horizontalAlignment: Text.AlignHCenter
				text: i18n.tr("ZenithGPS is an open source gps spoofer.<br>Community is what makes this app possible, so pull requests, translations, feedback and donations are very appreciated <br>This app stands on the shoulder of various Open Source projects, see source code for licensing details");
			}

			Label {
				anchors.horizontalCenter: parent.horizontalCenter
				width: parent.width * 4 / 5
				wrapMode: Text.WordWrap
				horizontalAlignment: Text.AlignHCenter
				text: i18n.tr("The app icon uses Suru icons (CC BY-SA 4.0)")
			}

			Label {
				anchors.horizontalCenter: parent.horizontalCenter
				width: parent.width * 4 / 5
				wrapMode: Text.WordWrap
				horizontalAlignment: Text.AlignHCenter
				text: i18n.tr("Thanks Brian Douglass for some Weather Indicator code")
			}

			Button {
				anchors.horizontalCenter: parent.horizontalCenter
				text: i18n.tr("❤Donate❤")
				color: UbuntuColors.green
				width: parent.width * 4 / 5
				onClicked: Qt.openUrlExternally("https://paypal.me/emanuele42");
			}

			Button {
				anchors.horizontalCenter: parent.horizontalCenter
				text: i18n.tr("See source on:") + " Gitlab"
				width: parent.width * 4 / 5
				onClicked: Qt.openUrlExternally("https://gitlab.com/tronfortytwo/zenithgps");
			}

			Button {
				anchors.horizontalCenter: parent.horizontalCenter
				text: i18n.tr("Report bug or feature request")
				width: parent.width * 4 / 5
				onClicked: Qt.openUrlExternally("https://gitlab.com/TronFortyTwo/zenithgps/-/issues");
			}

			Label {
				anchors.horizontalCenter: parent.horizontalCenter
				width: parent.width * 4 / 5
				wrapMode: Text.WordWrap
				horizontalAlignment: Text.AlignHCenter
				text: "Copyright (C) 2021 Emanuele Sorce (emanuele.sorce@hotmail.com)"
			}
		}
	}
}
