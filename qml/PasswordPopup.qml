import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Indicator 1.0

Item {
	id: passwordPopup
	
	width: units.gu(90)
	height: units.gu(90)
	
	signal aborted()
	signal wrongPassword()
	
	property string passwd: ""
	property string callback: "not-set"
	
	property string lat: "42"
	property string lon: "42"
	
	function open() {
		PopupUtils.open(dialogComponent)
	}
	
	function continuePressed() {
		
		if ( !Indicator.checkPassword(passwd) ) {
			wrongPassword();
			return;
		}
		
		if(callback === "activate")
			Indicator.activate(passwd, lat, lon);
		else if(callback === "deactivate")
			Indicator.deactivate(passwd);
		else
			console.log("Unknown callback: " + callback)
	}
	
	Component {
		id: dialogComponent
		
		Dialog {
			id: dialog
			title: i18n.tr("Authorization required")
			text: i18n.tr("To continue write your password")
			
			TextField {
				placeholderText: "Password"
				onTextChanged: passwordPopup.passwd = text
				echoMode: TextInput.Password
			}
			
			Button {
				text: i18n.tr("Authenticate")
				color: UbuntuColors.green
				onClicked: {
					passwordPopup.continuePressed();
					PopupUtils.close(dialog);
				}
			}
			
			Button {
				text: i18n.tr("Cancel")
				onClicked: {
					PopupUtils.close(dialog);
					aborted();
				}
			}
		}
	}
	
	
	
	
}
