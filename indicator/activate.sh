#!/bin/bash
echo "$1" | sudo -S service ubuntu-location-service stop

echo "$1" | sudo -S setprop custom.location.fake true
echo "$1" | sudo -S setprop custom.location.lat "$2"
echo "$1" | sudo -S setprop custom.location.lon "$3"

echo "$1" | sudo -S start ubuntu-location-service
