#!/bin/bash

echo "$1" | sudo -S service ubuntu-location-service stop

echo "$1" | sudo -S setprop custom.location.fake false

echo "$1" | sudo -S start ubuntu-location-service

echo "deactivated"
