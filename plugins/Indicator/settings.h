#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>

class Settings : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString lat MEMBER m_lat NOTIFY latChanged)
    Q_PROPERTY(QString lng MEMBER m_lng NOTIFY lngChanged)

public:
    Settings();
    ~Settings() = default;

    Q_INVOKABLE void save();

Q_SIGNALS:
    void saved(bool success);

    void latChanged(const QString &lat);
    void lngChanged(const QString &lng);

private:
    QString m_configPath = "/home/phablet/.config/zenithgps.emanuelesorce/"; // TODO don't hardcode this

    QString m_lat;
    QString m_lng;
};

#endif
