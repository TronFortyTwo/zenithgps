#include <QDebug>
#include <QCoreApplication>
#include <QFileInfo>
#include <QThread>

#include "indicator.h"

Indicator::Indicator() : m_activateProcess(),
						 m_deactivateProcess()
{
	// connect(&m_installProcess, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(onInstallFinished(int, QProcess::ExitStatus)));
	// connect(&m_uninstallProcess, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(onUninstallFinished(int, QProcess::ExitStatus)));
	connect(&m_activateProcess, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(onActivateFinished(int, QProcess::ExitStatus)));
	connect(&m_deactivateProcess, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(onDeactivateFinished(int, QProcess::ExitStatus)));

	checkInstalled();
}

void Indicator::activate(const QString &rootpw, const QString &lat, const QString &lon)
{
	m_activateProcess.start("bash /opt/click.ubuntu.com/zenithgps.emanuelesorce/current/indicator/activate.sh " + rootpw + " " + lat + " " + lon);
}

void Indicator::deactivate(const QString &rootpw)
{
	m_deactivateProcess.start("bash /opt/click.ubuntu.com/zenithgps.emanuelesorce/current/indicator/deactivate.sh " + rootpw);
}

bool Indicator::checkPassword(const QString &rootpw)
{
	QProcess proc;
	proc.start("bash /opt/click.ubuntu.com/zenithgps.emanuelesorce/current/indicator/checkpassword.sh " + rootpw);
	proc.waitForFinished(2000);

	QString out = proc.readAllStandardOutput();

	qDebug() << "checkpassword finished";
	qDebug() << "stdout" << out;
	qDebug() << "stderr" << proc.readAllStandardError();
	qDebug() << "exit code" << proc.exitCode() << "exit status" << proc.exitStatus();

	return out.contains("root");
}

void Indicator::onActivateFinished(int exitCode, QProcess::ExitStatus exitStatus)
{
	qDebug() << "install finished";
	qDebug() << "stdout" << m_activateProcess.readAllStandardOutput();
	qDebug() << "stderr" << m_activateProcess.readAllStandardError();
	qDebug() << "exit code" << exitCode << "exit status" << exitStatus;

	checkInstalled();
	Q_EMIT installed(exitCode == 0 && exitStatus == QProcess::NormalExit);
}

void Indicator::onDeactivateFinished(int exitCode, QProcess::ExitStatus exitStatus)
{
	qDebug() << "uninstall finished";
	qDebug() << "stdout" << m_deactivateProcess.readAllStandardOutput();
	qDebug() << "stderr" << m_deactivateProcess.readAllStandardError();
	qDebug() << "exit code" << exitCode << "exit status" << exitStatus;

	checkInstalled();
	Q_EMIT uninstalled(exitCode == 0 && exitStatus == QProcess::NormalExit);
}

bool Indicator::checkInstalled()
{

	QProcess proc;
	proc.start("getprop custom.location.fake");
	proc.waitForFinished(2000);

	QString out = proc.readAllStandardOutput();

	qDebug() << "checkinstalled finished";
	qDebug() << "stdout" << out;
	qDebug() << "stderr" << proc.readAllStandardError();
	qDebug() << "exit code" << proc.exitCode() << "exit status" << proc.exitStatus();

	m_isInstalled = out.contains("true");
	Q_EMIT isInstalledChanged(m_isInstalled);

	return m_isInstalled;
}
