#include <QDebug>
#include <QFile>
#include <QDir>
#include <QJsonDocument>
#include <QJsonObject>
#include <QFileInfo>

#include "settings.h"

Settings::Settings() {
    QFile config(m_configPath + "config.json");
    config.open(QFile::ReadOnly);

    QJsonDocument doc = QJsonDocument::fromJson(config.readAll());
    QJsonObject object = doc.object();

    m_lat = object.value("lat").toString().trimmed();
    m_lng = object.value("lng").toString().trimmed();

    Q_EMIT latChanged(m_lat);
    Q_EMIT lngChanged(m_lng);
	
    config.close();
}

void Settings::save() {
    QJsonObject object;
    object.insert("lat", QJsonValue(m_lat.trimmed()));
    object.insert("lng", QJsonValue(m_lng.trimmed()));

    QJsonDocument doc;
    doc.setObject(object);

    if (!QDir(m_configPath).exists()) {
        QDir().mkdir(m_configPath);
    }

    QFile config(m_configPath + "config.json");
    bool success = config.open(QFile::WriteOnly | QFile::Text | QFile::Truncate);
    config.write(doc.toJson());
    config.close();

    Q_EMIT saved(success);
}
