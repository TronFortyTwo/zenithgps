#ifndef INDICATOR_H
#define INDICATOR_H

#include <QObject>
#include <QProcess>
#include <QString>

class Indicator: public QObject {
    Q_OBJECT

    Q_PROPERTY(bool isInstalled MEMBER m_isInstalled NOTIFY isInstalledChanged)

public:
    Indicator();
    ~Indicator() = default;

	Q_INVOKABLE void activate(const QString& rootpw, const QString& lat, const QString& lon);
	Q_INVOKABLE void deactivate(const QString& rootpw);
	Q_INVOKABLE bool checkPassword(const QString& rootpw);

	Q_INVOKABLE
	bool checkInstalled();

Q_SIGNALS:
    void installed(bool success);
    void uninstalled(bool success);

    void isInstalledChanged(const bool isInstalled);

private Q_SLOTS:
	void onActivateFinished(int exitCode, QProcess::ExitStatus exitStatus);
	void onDeactivateFinished(int exitCode, QProcess::ExitStatus exitStatus);

private:
    Q_DISABLE_COPY(Indicator)

    QProcess m_activateProcess;
    QProcess m_deactivateProcess;
	
    bool m_isInstalled = false;
};

#endif
